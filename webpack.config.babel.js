import webpack      from 'webpack';
import path         from 'path';
import autoprefixer from 'autoprefixer';
import precss       from 'precss';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src');
const REAL_ENV = (new String(process.env.NODE_ENV)).trim().toLowerCase();
var IS_DEBUG = !(REAL_ENV  === 'production');

function getPlugins(){
    let plugins = [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(REAL_ENV)
            }
        }),
        new ExtractTextPlugin("bundle.css",  {
            allChunks: true
        }),
        getImplicitGlobals()
    ];

    if (!IS_DEBUG){
        plugins = [...plugins,
            new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|ru/),


            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.UglifyJsPlugin({
                compress: { warnings: false },
                comments: false,
                sourceMap: false,
                mangle: true,
                minimize: true
            }),
        ];
    }

    return plugins;
}

var config = {
    entry: ['whatwg-fetch', APP_DIR + '/index.jsx'],
    devtool: IS_DEBUG ? 'source-map' : 'cheap-module-source-map',
    output: {
        path: BUILD_DIR,
        publicPath: IS_DEBUG ? 'http://localhost:8080/public/' : "/",
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loader: 'babel'
            }, {
                test: /\.css$/,
                loader: IS_DEBUG ? "css-loader!postcss" : ExtractTextPlugin.extract("style-loader", "css-loader!postcss")
            }, {
                test: /\.scss$/,
                loader:  ExtractTextPlugin.extract("style-loader", "sass-loader!postcss")
            }, {
                test: /\.less$/,
                loader: IS_DEBUG ? "less-loader!postcss" : ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
            }, {
                test: /\.json$/,
                loader: 'json'
            }, {
                test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
                loader: 'url?limit=100000&name=[name].[ext]'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
    },
    plugins: getPlugins(),
    postcss: function () {
        return [precss, autoprefixer];
    }
};

function getImplicitGlobals() {
    return new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
    });
}

module.exports = config;