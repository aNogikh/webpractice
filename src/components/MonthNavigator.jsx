import React from 'react';
import moment from 'moment';
import Month from './Month';

import 'bootstrap/dist/css/bootstrap.min.css';


const MonthNames = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Авгут","Сентябрь","Октябрь","Ноябрь","Декабрь"];

class MonthNavigator extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            year: props.year || moment().year(),
            month: props.month || moment().month()
        };

        this.changeMonth = this.changeMonth.bind(this);
    }

    changeMonth(diff){
        return () => {
            let x = this.state.year * 12 + this.state.month - 1 + diff;
            this.setState(Object.assign(this.state, {
                year: Math.floor(x/12),
                month: (x % 12) + 1
            }));
        };
    }

    render(){
        return <div className="CalendarNavigator">
            <div className="CalendarNavigator_Head">
                <button type="button" className="btn btn-default CalendarNavigator_Head_Left" onClick={this.changeMonth(-1)}>
                    <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
                </button>
                <span>{MonthNames[this.state.month - 1]} {this.state.year}</span>
                <button type="button" className="btn btn-default CalendarNavigator_Head_Right" onClick={this.changeMonth(1)}>
                    <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </button>
            </div>
            <Month year={this.state.year} month={this.state.month} />
        </div>
    }
}

MonthNavigator.propTypes = {
    month: React.PropTypes.number,
    year: React.PropTypes.number
};

export default MonthNavigator;