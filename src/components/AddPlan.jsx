import React from 'react';
import Formsy from 'formsy-react';
import ValidatedTextBox from '../utils/ValidatedTextBox';

class AddPlan extends React.Component{
    constructor(props) {
        super(props);
        this.state = {canSubmit: false};
        this.reset = this.reset.bind(this);
    }

    enableButton() {
        this.setState({
            canSubmit: true
        });
    }

    disableButton() {
        this.setState({
            canSubmit: false
        });
    }

    handleSubmit(model){
        (this.props.onSubmit || (() => 0))(model.text);
    }

    reset(){
        this.refs.form.reset();
    }

    render(){
        let input;
        return <div>
            <Formsy.Form
                ref="form"
                onValid={this.enableButton.bind(this)}
                onInvalid={this.disableButton.bind(this)}
                onValidSubmit={this.handleSubmit.bind(this)}
            >
                <div className="form-group">
                    <label>Текст:</label>
                    <ValidatedTextBox type="text"
                                      className="form-control"
                                      name="text"
                                      required
                    />
                </div>

                <button type="submit"
                        className="btn btn-default"
                        disabled={!this.state.canSubmit}
                >Добавить</button>
            </Formsy.Form>

        </div>
    }
}

export default AddPlan;