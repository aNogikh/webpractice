import React from 'react';
import Formsy from 'formsy-react';
import ValidatedTextBox from '../utils/ValidatedTextBox';

class LogInForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {canSubmit: false};
        this.enableButton = this.enableButton.bind(this);
        this.disableButton = this.disableButton.bind(this);
    }

    enableButton() {
        this.setState({
            canSubmit: true
        });
    }

    disableButton() {
        this.setState({
            canSubmit: false
        });
    }

    render(){
        return <div className={this.props.className}>
            <h3>Авторизация</h3>
            <Formsy.Form
                onValid={this.enableButton}
                onInvalid={this.disableButton}
                onValidSubmit={this.props.handleSend || function(){}}
            >
                <div className="form-group">
                    <label>E-mail:</label>
                    <ValidatedTextBox type="email"
                           className="form-control"
                           validations="isEmail"
                           validationError="Неверный e-mail!"
                           name="email"
                          value=""
                           required
                    />
                </div>
                <div className="form-group">
                    <label>Пароль:</label>
                    <ValidatedTextBox type="password"
                           className="form-control"
                           name="password"
                          value=""
                           required
                    />
                </div>
                <button type="submit" className="btn btn-default" disabled={!this.state.canSubmit}>Отправить</button>
            </Formsy.Form>

        </div>
    }
}

export default LogInForm;