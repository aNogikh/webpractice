import React from 'react';
import moment from 'moment';
import DayLine from './DayLine';

class Month extends React.Component{
    render(){
        let month = this.props.month - 1;
        let weeks = [], week = [];

        let now = moment().month(month).year(this.props.year).date(1);
        for (;now.month() == month; now = now.add(1, 'd')) {
            if (week.length >= 7) {
                weeks.push(week);
                week = [];
            }

            week.push(now.clone());
        }

        if (week.length)
            weeks.push(week);

        return <span>{weeks.map(function(v, i){
            return <DayLine days={v} key={i} />;
        })}</span>
    }
}

Month.propTypes = {
    month: React.PropTypes.number,
    year: React.PropTypes.number
};

export default Month;