import React from 'react';
import Day from './Day';

class DayLine extends React.Component{
    render(){
        return <div>{this.props.days.map(function(v, i){
            return <Day day={v} key={i} />;
        })}</div>
    }
}

DayLine.propTypes = {
    days: React.PropTypes.array
};

export default DayLine;