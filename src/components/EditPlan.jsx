import React from 'react';
import Formsy from 'formsy-react';
import ValidatedTextBox from '../utils/ValidatedTextBox';

class EditPlan extends React.Component{
    constructor(props) {
        super(props);
        this.state = {canSubmit: false};
    }

    enableButton() {
        this.setState({
            canSubmit: true
        });
    }

    disableButton() {
        this.setState({
            canSubmit: false
        });
    }

    handleSubmit(model){
        (this.props.onSubmit || (() => 0))({text: model.text});
    }

    render(){
        return <div>
            <Formsy.Form
                ref="form"
                className="form-inline row"
                onValid={this.enableButton.bind(this)}
                onInvalid={this.disableButton.bind(this)}
                onValidSubmit={this.handleSubmit.bind(this)}
            >
                <div className="form-group col-lg-9">
                    <ValidatedTextBox type="text"
                                      className="form-control PlanEditField"
                                      name="text"
                                      value={this.props.text}
                                      required
                    />
                </div>
                &nbsp;
                <button type="submit"
                        className="btn btn-default"
                        disabled={!this.state.canSubmit}
                >Сохранить</button>
                &nbsp;
                <a onClick={() => (this.props.cancelledButtonClicked || function(){} )()}>Cancel</a>
            </Formsy.Form>

        </div>
    }
}

export default EditPlan;