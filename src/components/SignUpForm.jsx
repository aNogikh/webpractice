import React from 'react';
import Formsy from 'formsy-react';
import ValidatedTextBox from '../utils/ValidatedTextBox';

class SignUpForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {canSubmit: false};
        this.enableButton = this.enableButton.bind(this);
        this.disableButton = this.disableButton.bind(this);

    }

    enableButton() {
        this.setState({
            canSubmit: true
        });
    }

    disableButton() {
        this.setState({
            canSubmit: false
        });
    }

    render(){
        return <div className={this.props.className}>
            <h3>Регистрация</h3>
            <Formsy.Form
                onValid={this.enableButton}
                onInvalid={this.disableButton}
                onValidSubmit={this.props.handleSend || function(){}}
            >
                <div className="form-group">
                    <label>Имя:</label>
                    <ValidatedTextBox type="text"
                                      className="form-control"
                                      validations={{
                                        minLength: 2,
                                        maxLength: 20
                                      }}
                                      validationErrors={{
                                        minLength: "Должно быть хотя бы два символа!",
                                        maxLength: "Не более 20 символов!"
                                      }}
                                      name="name"
                                      required
                    />
                </div>
                <div className="form-group">
                    <label>E-mail:</label>
                    <ValidatedTextBox type="email"
                                      className="form-control"
                                      validations="isEmail"
                                      validationError="Неверный e-mail!"
                                      name="email"
                                      required
                    />
                </div>
                <div className="form-group">
                    <label>Пароль:</label>
                    <ValidatedTextBox type="password"
                                      className="form-control"
                                      name="pwd"
                                      required
                    />
                </div>
                <button type="submit" className="btn btn-default" disabled={!this.state.canSubmit}>Зарегистрироваться</button>
            </Formsy.Form>

        </div>
    }
}

export default SignUpForm;