import React from 'react'
import PlanContainer from '../containers/PlanContainer';
const PropTypes = React.PropTypes;

const PlanList = ({plans}) => {
    let sortedPlans = plans.sort((x, y) => {
        if (x.completed && !y.completed)
            return 1;
        if (!x.completed && y.completed)
            return -1;
        return 0;
    });

    return <ul className="list-group">
        {plans.map(plan => <li
            className="list-group-item"
            key={plan.id}
        >
            <PlanContainer
                plan = {plan}
            />
        </li>)}
    </ul>
};

PlanList.propTypes = {
    plans: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        completed: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired).isRequired
};

export default PlanList;