import React from 'react'
import classNames from 'classnames';
const PropTypes = React.PropTypes;

let PlanText =({text, completed}) => (
    completed ? <s>{text}</s> : <span>{text}</span>
);

const Plan = function({id, text, completed, editPlan, deletePlan, changeCompleteness}){
    let planActions = [];
    if (!completed){
        planActions = [
            <a className="btn btn-default btn-circle"
                onClick={() => (editPlan || function(){} )()}
                key="edit"
            >
                <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>,
            <a className="btn btn-default btn-circle"
               onClick={() => (deletePlan || function(){} )()}
                key="delete"
            >
                <i className="fa fa-trash" aria-hidden="true"></i>
            </a>
        ];
    }

    return <div>
        <span className="PlanActions Left">
            <a className="btn btn-default btn-circle"
               onClick={() => (changeCompleteness || function(){} )(!completed)}>
                <i className={classNames('fa', {
                    'fa-check': !completed,
                    'fa-undo': completed
                })} aria-hidden="true"></i>
            </a>
            &nbsp;
        </span>
        <PlanText text={text} completed={completed} />

        <span className="PlanActions Right">
            {planActions}
        </span>
    </div>
};

Plan.propTypes = {
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired
};

export default Plan;