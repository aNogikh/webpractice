import React from 'react';

export default ({text, onCancel}) => (
    <div className="alert alert-danger" role="alert">
        <button type="button"
                className="close"
                aria-label="Close"
                onClick={ onCancel || function(){} }
        >
            <span aria-hidden="true">&times;</span>
        </button>
        {text}
    </div>
);