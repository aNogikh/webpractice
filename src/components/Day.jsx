import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import {setCurrentDay} from '../actions';
import classNames from 'classnames';

class Day extends React.Component{
    setAsCurrent(){
        this.props.dispatch(setCurrentDay(this.props.day));
    }
    render(){
        let dayPlans = this.props.dayPlans;
        let doneCount = dayPlans.filter(plan => plan.completed).length,
            waitingCount =  dayPlans.filter(plan => !plan.completed).length;

        let blockClasses = classNames({
            'Calendar_Day': true,
            'empty': dayPlans.length == 0,
            'attention': waitingCount > 0 && doneCount == 0,
            'completed': waitingCount == 0 && doneCount > 0,
            'partially': waitingCount > 0 && doneCount > 0,
            'today': moment().isSame(this.props.day, 'day'),
            'current': this.props.day.isSame(this.props.currentDay, 'day')
        });

        return <span
            className={blockClasses}
            onClick={this.setAsCurrent.bind(this)}
        >
            {this.props.day.date()}
            {waitingCount > 0 ? <span className="label label-danger">{waitingCount}</span> : ''}
        </span>
    }
}

Day.propTypes = {
    day: React.PropTypes.instanceOf(moment)
};

export default connect(
    (state, ownProps) => ({
        dayPlans:  state.plans.filter(plan => plan.day.isSame(ownProps.day, 'day')),
        currentDay: state.currentDay
    })
)(Day);