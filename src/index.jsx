import React from 'react';
import {render} from 'react-dom';
import {Router, browserHistory, hashHistory, useRouterHistory } from 'react-router';
import routes from './routes';
import { Provider } from 'react-redux'
import configureStore from './configureStore';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'babel-polyfill';
import 'animate.css';
import 'jquery';
import 'font-awesome/css/font-awesome.min.css';
import './style.less';

const store = configureStore();

render(
    <Provider store={store}>
        <Router history={hashHistory}  routes={routes}/>
    </Provider>,
    document.getElementById('app')
);