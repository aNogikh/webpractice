import React from 'react'
import { Route, IndexRedirect, Redirect } from 'react-router'
import App from './containers/App';
import LogInContainer from './containers/LogInCointaner';
import SignUpContainer from './containers/SignUpCointaner';
import PlansPage from './containers/PlansPage';

export default <Route path="/" component={App}>
    <IndexRedirect to="/log_in" />
    <Route path="sign_up" component={SignUpContainer} />
    <Route path="log_in" component={LogInContainer} />
    <Route path="plans" component={PlansPage} />
    <Redirect from="*" to="/" />
</Route>