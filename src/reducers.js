import { combineReducers } from 'redux'
import {
    CREATE_PLAN, EDIT_PLAN, REMOVE_PLAN, SET_COMPLETENESS,
    SET_PLANS, SET_CURRENT_DAY, SET_LOGGED_USER,
    ADD_ERROR, DISMISS_ERROR, CLEAR_ERRORS
} from './actions'
import moment from 'moment'

function handleCurrentDay(state = null, action){
    switch (action.type){
        case SET_CURRENT_DAY:
            return action.day;
    }

    if (state == null)
        return moment();

    return state;
}

function handlePlans(state = [], action){
    switch(action.type){
        case SET_PLANS:
            return action.plans;
        case CREATE_PLAN:
            return [...state, action.data];
        case EDIT_PLAN:
            return state.map(plan => {
                if (plan.id != action.id)
                    return plan;
                return Object.assign(plan, action.data);
            });
        case REMOVE_PLAN:
            return state.filter(plan => plan.id != action.id);
        case SET_COMPLETENESS:
            return state.map(plan => {
                if (plan.id != action.id)
                    return plan;
                return Object.assign(plan, {
                    completed: action.completed
                })
            })
    }

    return state;
}

function handleUser(state = null, action){
    switch(action.type){
        case SET_LOGGED_USER:
            return action.user;
    }

    return state;
}

function handleErrors(state = [], action) {
    switch (action.type) {
        case CLEAR_ERRORS:
            return [];
        case ADD_ERROR:
            return Array.isArray(action.text) ?
                [...state, ...action.text] :
                [...state, action.text];
        case DISMISS_ERROR:
            return state.filter((text, i) => i != action.id);
    }

    return state;
}

const rootReducer = combineReducers({
    currentDay: handleCurrentDay,
    plans: handlePlans,
    user: handleUser,
    errors: handleErrors
});

export default rootReducer