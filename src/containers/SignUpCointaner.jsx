import SignUpForm from '../components/SignUpForm';
import React from 'react';
import { connect } from 'react-redux';
import { signUp, clearErrors } from '../actions';

class SignUpContainer extends React.Component{
    constructor(props){
        super(props);
        this.handleSend = this.handleSend.bind(this);
    }
    
    handleSend(data){
        this.props.dispatch(clearErrors());
        this.props.dispatch(signUp(data.name, data.email, data.pwd));
    }

    render(){
        return <SignUpForm
            ref="form"
            className="UserForm"
            handleSend={this.handleSend}
        />
    }
}

export default connect(
    (state, ownProps) => ({user: state.user})
)(SignUpContainer);