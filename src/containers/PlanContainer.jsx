import React from 'react';
import Plan from '../components/Plan';
import EditPlan from '../components/EditPlan';
import { connect } from 'react-redux';
import { updatePlan, deletePlan, completePlan } from '../actions';

class PlanContainer extends React.Component{
    constructor(props){
        super(props);
        this.state = {editing: false};
        this.changeEditingMode = this.changeEditingMode.bind(this);
    }

    changeEditingMode(newMode){
        return () => {
            this.setState({editing: newMode});
        }
    }

    onEditSave(diff){
        this.props.dispatch(updatePlan(this.props.plan.id, diff.text));
        this.changeEditingMode(false)();
    }
    
    deletePlan(){
        this.props.dispatch(deletePlan(this.props.plan.id));
    }

    completePlan(newState){
        this.props.dispatch(completePlan(this.props.plan.id, newState));
    }

    render(){
        return this.state.editing ?
            <EditPlan
                cancelledButtonClicked={this.changeEditingMode(false)}
                onSubmit={this.onEditSave.bind(this)}
                {...this.props.plan} /> :
            <Plan
                editPlan={this.changeEditingMode(true)}
                deletePlan={this.deletePlan.bind(this)}
                changeCompleteness={this.completePlan.bind(this)}
                {...this.props.plan}
            />
    }
}

// we need it to supply dispatch method and an implementation for shouldComponentUpdate
export default connect(
    (state, ownProps) => ({originalPlan: state.plans.filter(plan => plan.id == ownProps.plan.id)})
)(PlanContainer);