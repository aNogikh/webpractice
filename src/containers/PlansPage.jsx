import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import CurrentDay from './CurrentDay';
import MonthNavigator from '../components/MonthNavigator';

class PlansPage extends React.Component{
    render(){
        let now = moment();

        return <div>
            <MonthNavigator month={now.month() + 1} year={now.year()}/>
            {this.props.currentDay ? <CurrentDay day={this.props.currentDay} /> : ''}
        </div>
    }
}

export default connect(
    (state, ownProps) => ({ currentDay: state.currentDay })
)(PlansPage);