import React from 'react';
import PlanList from '../components/PlanList';
import AddPlan from '../components/AddPlan';
import {createPlan} from '../actions';
import { connect } from 'react-redux';

class CurrentDay extends React.Component {
    addPlan(text) {
        this.props.dispatch(createPlan(this.props.day, text));
        this.refs.add.reset();
    }

    render() {
        let plans = this.props.plans.filter(plan => plan.day.isSame(this.props.day, 'day'));

        return <div className="CurrentDay">
            <h4>Планы на {this.props.day.format('YYYY-MM-DD')}</h4>
            <PlanList plans={plans}/>
            <AddPlan onSubmit={this.addPlan.bind(this)} ref="add"/>
        </div>
    }
}
export default connect(
    (state, ownProps) => ({plans:  state.plans})
)(CurrentDay);