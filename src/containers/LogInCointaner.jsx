import LogInForm from '../components/LogInForm';
import React from 'react';
import { connect } from 'react-redux';
import { logIn, clearErrors } from '../actions';

class LogInContainer extends React.Component{
    constructor(props){
        super(props);
        this.handleSend = this.handleSend.bind(this);
    }

    handleSend(data){
        this.props.dispatch(clearErrors());
        this.props.dispatch(logIn(data.email, data.password));
    }

    render(){
        return <LogInForm
            ref="form"
            className="UserForm"
            handleSend={this.handleSend}
        />
    }
}

export default connect(
    (state, ownProps) => ({user: state.user})
)(LogInContainer);