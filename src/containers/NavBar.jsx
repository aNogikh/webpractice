import React from 'react';
import {Link} from 'react-router';
import { connect } from 'react-redux';

class NavBar extends React.Component {
    render() {
        return <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <a className="navbar-brand" href="#">
                        Organizer
                    </a>
                </div>
                    {this.props.user ? <p className="navbar-text">
                        Добро пожаловать, {this.props.user.name}!
                        &nbsp;


                    </p> : <span>
                        <Link to="/log_in" className="btn btn-default navbar-btn">Войти</Link>
                        &nbsp;
                        <Link to="/sign_up" className="btn btn-default navbar-btn">Зарегистрироваться</Link>
                    </span>}

                    {this.props.user ? <ul className="nav navbar-nav navbar-right">
                        <li>
                            <button
                                onClick={this.props.logOutAction || function(){}}
                                className="btn btn-default navbar-btn LogOutButton"
                            >
                                Выйти
                            </button>
                        </li>
                    </ul> : ''}


            </div>
        </nav>
    }
}

export default NavBar;