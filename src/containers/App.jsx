import React from 'react';
import {Link, withRouter} from 'react-router';
import { connect } from 'react-redux';
import NavBar from './NavBar';
import ErrorList from './ErrorList';
import { checkLoggedUser, logOut, fetchPlans, clearErrors } from '../actions';

class App extends React.Component{
    componentWillMount() {
        this.props.dispatch(checkLoggedUser());
    }

    logOutAction(){
        this.props.dispatch(logOut());
    }

    componentWillReceiveProps(newProps) {
        if (!this.props.user && newProps.user) {
            this.props.router.push('/plans');
            this.props.dispatch(fetchPlans());
            this.props.dispatch(clearErrors());
        }

        if (!newProps.user && this.props.user){
            this.props.router.push('/log_in');
        }
    }

    render(){
        return ( <div className="container">
            <NavBar
                user={this.props.user}
                logOutAction={this.logOutAction.bind(this)}
            />
            <ErrorList />
            {this.props.children}
            </div>)
    }
}


export default withRouter(connect(
    (state, ownProps) => ({user: state.user})
)(App));