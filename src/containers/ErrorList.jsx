import React from 'react';
import Error from '../components/Error';
import { connect } from 'react-redux';
import { dismissError } from '../actions';

class ErrorList extends React.Component{
    dropError(id){
        this.props.dispatch(dismissError(id));
    }
    render(){
        return <div>
            {this.props.errors.map((error,i) => (
                <Error
                    key={"Error" + i}
                    text={error}
                    onCancel={ (() => this.dropError(i)) }
                />
            ))}
        </div>
    }
}

export default connect(
    (state, ownProps) => ({errors: state.errors})
)(ErrorList);