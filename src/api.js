import moment from 'moment';
import 'whatwg-fetch';

const ACTIONS_BASE = 'actions';

function noAction(){}

export function CheckLoggedUser(success){
    makeRequest('GET', ACTIONS_BASE + "/info.php")
        .then((json) => json ? success(json) : 1)
        .catch(() => 1);
}

export function LogIn(params, success = noAction, fail = noAction) {
    makeRequest('POST', ACTIONS_BASE + '/login.php', {
        email: params.email,
        password: params.password
    }, true)
        .then((response) => {
            if (response.status != 200){
                fail('Неверные данные!');
                return;
            }

            return response.json().then((json) => success(json));
        })
        .catch((error) => fail(error.message));
}

export function SignUp(params, success = noAction, fail = noAction){
    makeRequest('POST', ACTIONS_BASE + '/register.php', {
        email: params.email,
        name: params.name,
        password: params.password
    })
        .then((json) => success(json))
        .catch((error) => fail(error.message));
}

export function LogOut(success = noAction, fail = noAction){
    makeRequest('POST', ACTIONS_BASE + '/logout.php')
        .then(() => success())
        .catch(() => fail('Возникла ошибка!'));
}

function convertPlan({id, text, time, done}) {
    return {
        id, text,
        completed: done ? true : false,
        day: moment(time)
    }
}

export function LoadPlans(success = noAction, fail = noAction){
    makeRequest('GET', ACTIONS_BASE + '/viewtask.php')
        .then((json) => {
            if (!Array.isArray(json))
                return [];

            return json.map(convertPlan);
        })
        .then((array) => success(array))
        .catch((error) => fail(error));
}

export function CreatePlan(day, text, success = noAction, fail = noAction) {
    makeRequest('POST', ACTIONS_BASE + "/addtask.php", {
        text: text,
        date: day.format('YYYY-MM-DD H:mm')
    })
        .then((json) => success(convertPlan(json)))
        .catch((error) => fail(error));
}

export function UpdatePlan(id, params, success = noAction, fail = noAction){
    makeRequest('POST', ACTIONS_BASE + "/edittask.php", {
        text: params.text,
        id
    })
        .then((json) => success())
        .catch((error) => fail(error));
}

export function DeletePlan(id, success = noAction, fail = noAction) {
    makeRequest('POST', ACTIONS_BASE + "/deletetask.php", {
        id
    })
        .then((json) => success())
        .catch((error) => fail(error));
}

export function CompletePlan(id, newComplete, success = noAction, fail = noAction){
    makeRequest('POST', ACTIONS_BASE + "/statustask.php", {
        done: newComplete ? 1 : 0,
        id
    })
        .then((json) => success())
        .catch((error) => fail(error));
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        return response.json().then((json) => {
            if (typeof json === "string") {
                throw new Error(json);
            } else {
                let keys = Object.keys(json);
                if (keys.length > 0)
                    throw new Error(`${keys[0]}: ${json[keys[0]]}`);
            }

            throw new Error("Some other error");
        });
    }
}

function makeRequest(method, url, data, returnRaw = false) {
    let formData = new FormData();
    for (let key in (data || {})) {
        if (data.hasOwnProperty(key))
            formData.append(key, data[key]);
    }

    let config = {
        method: method,
        credentials: 'include'
    };

    if (method.toString().toLowerCase() == 'post')
        config['body'] = formData;

    let instance = fetch(url, config);
    if (returnRaw)
        return instance;

    return instance
        .then(checkStatus)
        .then(response => response.json())
}
