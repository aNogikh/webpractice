import * as api from './api';

export const SET_LOGGED_USER = 'SET_LOGGED_USER';
export const CREATE_PLAN = 'CREATE_PLAN';
export const EDIT_PLAN = 'EDIT_PLAN';
export const REMOVE_PLAN = 'REMOVE_PLAN';
export const SET_COMPLETENESS = 'SET_COMPLETENESS';
export const SET_PLANS = 'SET_PLANS';
export const SET_CURRENT_DAY = 'SET_CURRENT_DAY';
export const ADD_ERROR = 'ADD_ERROR';
export const DISMISS_ERROR = 'DISMISS_ERROR';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

export function addError(text){
    return {
        type: ADD_ERROR,
        text
    }
}

export function dismissError(i){
    return {
        type: DISMISS_ERROR,
        id: i
    }
}

export function clearErrors(){
    return {
        type: CLEAR_ERRORS,
    }
}

export function setLoggedUser(user){
    return {
        type: SET_LOGGED_USER,
        user
    }
}

export function setCurrentDay(day){
    return {
        type: SET_CURRENT_DAY,
        day
    }
}

function createPlanAction(data){
    return {
        type: CREATE_PLAN,
        data
    }
}

function editPlanAction(id, data){
    return {
        type: EDIT_PLAN,
        id,
        data
    }
}

function deletePlanAction(id){
    return {
        type: REMOVE_PLAN,
        id
    }
}

function setCompleteness(id, completed){
    return {
        type: SET_COMPLETENESS,
        id,
        completed
    }
}

function setPlans(plans){
    return {
        type: SET_PLANS,
        plans
    }
}

export function checkLoggedUser(){
    return (dispatch) => {
        api.CheckLoggedUser(
            (data) => dispatch(setLoggedUser(data))
        )
    }
}

export function fetchPlans(){
    return (dispatch) => {
        api.LoadPlans(
            (data) => dispatch(setPlans(data))
        );
    };
}

export function createPlan(day, text, success){
    return (dispatch) => {
        api.CreatePlan(day, text,
            (data) => dispatch(createPlanAction(data))
        );
    };
}

export function updatePlan(id, text){
    return (dispatch) => {
        api.UpdatePlan(id, {text},
            () => dispatch(editPlanAction(id, {text}))
        );
    };
}

export function deletePlan(id){
    return (dispatch) => {
        api.DeletePlan(id,
            () => dispatch(deletePlanAction(id))
        );
    };
}

export function completePlan(id, completed){
    return (dispatch) => {
        api.CompletePlan(id, completed,
            () => dispatch(setCompleteness(id, completed))
        )
    };
}

export function signUp(name, email, password){
    return (dispatch) => {
        api.SignUp({name, email, password},
            (data) => dispatch(setLoggedUser(data)),
            (error) => dispatch(addError(error))
        );
    };
}

export function logIn(email, password) {
    return (dispatch) => {
        api.LogIn({email, password},
            (data) => dispatch(setLoggedUser(data)),
            (error) => dispatch(addError(error))
        );
    }
}

export function logOut(){
    return (dispatch) => {
        api.LogOut(
            (data) => dispatch(setLoggedUser(null)),
            (error) => dispatch(addError(error))
        );
    };
}